import { Button, Card } from 'react-bootstrap';
import { useState } from 'react';

export default function CourseCard(prop) {
    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);

    function seatOccupation() {
        if (count === 30) {
            return alert("No more seats");
        }

        setCount(count + 1);
        setSeats(seats - 1);
    }

    return (

        <Card className="my-5 cardHighlight p-3">
            <Card.Body>
                <Card.Title>
                    <h2>{prop.name}</h2>
                </Card.Title>
                <Card.Text>
                    <strong>Description:</strong>
                    <p>{prop.description}</p>
                    <strong>Price:</strong>
                    <p>PHP {prop.price}</p>
                    <p><strong>Enrollees: {count}</strong></p>
                    <Button variant="primary" onClick={seatOccupation}>Enroll Now!</Button>
                </Card.Text>
            </Card.Body>
        </Card>

    )
}
